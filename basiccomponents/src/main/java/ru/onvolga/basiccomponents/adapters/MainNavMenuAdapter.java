package ru.onvolga.basiccomponents.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import ru.onvolga.basiccomponents.R;
import ru.onvolga.basiccomponents.R2;
import ru.onvolga.basiccomponents.mvp.PageElement;
import ru.onvolga.basiccomponents.utils.TextIconSimulate;
import ru.onvolga.basiccomponents.views.MenuIconTextView;

/**
 * Created by dzmitry on 27.2.17.
 */

public class MainNavMenuAdapter extends RealmRecyclerViewAdapter<PageElement, MainNavMenuAdapter.MainNavViewHolder> {

    private LayoutInflater mLayoutInflater;
    private IOnPageElementClickListener listener;

    public MainNavMenuAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<PageElement> data) {
        super(context, data, false);
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setListener(IOnPageElementClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MainNavViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainNavViewHolder(mLayoutInflater.inflate(R.layout.item_menu_nav, parent, false));
    }

    @Override
    public void onBindViewHolder(MainNavViewHolder holder, int position) {
        PageElement item = getItem(position);
        holder.menuText.setText(item.getName());
        holder.menuIcon.setText(TextIconSimulate.getMenuIcon(item.getIcon()));
        holder.itemView.setOnClickListener(view -> {
            if (listener != null) listener.onPageClick(item);
        });
    }




    public class MainNavViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.menuIcon)
        MenuIconTextView menuIcon;

        @BindView(R2.id.menuText)
        TextView menuText;

        public MainNavViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
