package ru.onvolga.basiccomponents.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.onvolga.basiccomponents.R;
import ru.onvolga.basiccomponents.R2;
import ru.onvolga.basiccomponents.mvp.ItemSectionData;
import ru.onvolga.basiccomponents.views.MenuIconTextView;

/**
 * Created by dzmitry on 28.2.17.
 */

public class ItemSectionDataItemAdapter extends RecyclerView.Adapter<ItemSectionDataItemAdapter.ItemSectionHolder> {

    LayoutInflater mLayoutInflater;
    List<ItemSectionData.ItemSectionDataItem> data;
    OnItemSectionClickListener onItemSectionClickListener;

    public ItemSectionDataItemAdapter(Context ctx, List<ItemSectionData.ItemSectionDataItem> data) {
        this.data = data;
        mLayoutInflater = LayoutInflater.from(ctx);
    }

    public void setOnItemSectionClickListener(OnItemSectionClickListener onItemSectionClickListener) {
        this.onItemSectionClickListener = onItemSectionClickListener;
    }

    @Override
    public ItemSectionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemSectionHolder(mLayoutInflater.inflate(R.layout.item_item_section, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemSectionHolder holder, int position) {
        ItemSectionData.ItemSectionDataItem item = data.get(position);
        holder.nameTextView.setText(item.getName());
        holder.itemView.setOnClickListener(v -> {
            if (onItemSectionClickListener != null) onItemSectionClickListener.onItemClick(item);
        });
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public interface OnItemSectionClickListener {
        void onItemClick(ItemSectionData.ItemSectionDataItem item);
    }

    public static class ItemSectionHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.nameTextView)
        TextView nameTextView;

        @BindView(R2.id.costTextView)
        TextView costTextView;
        @BindView(R2.id.itemImage)
        ImageView itemImage;
        @BindView(R2.id.arrowRight)
        MenuIconTextView arrowRight;

        public ItemSectionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
