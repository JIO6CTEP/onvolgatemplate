package ru.onvolga.basiccomponents.adapters;

import ru.onvolga.basiccomponents.mvp.PageElement;

/**
 * Created by dzmitry on 27.2.17.
 */

public interface IOnPageElementClickListener {
    void onPageClick(PageElement page);
}
