package ru.onvolga.basiccomponents.views;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by DmitrySobolevsky on 20.06.2016.
 */
public class DisableScrollView extends NestedScrollView {
    protected boolean isScrollEnabled = true;
    private GestureDetector mGestureDetector;

    public DisableScrollView(Context context) {
        super(context);
        inits(context);
    }

    public DisableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inits(context);
    }

    public DisableScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inits(context);
    }

    void inits(Context context) {
        mGestureDetector = new GestureDetector(context, new YScrollDetector());
        setFadingEdgeLength(0);
    }

    public void enableScroll(boolean isScrollEnabled) {
        this.isScrollEnabled = isScrollEnabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isScrollEnabled) {
            return super.onInterceptTouchEvent(ev)
                    && mGestureDetector.onTouchEvent(ev);
        } else {
            return false;
        }
    }

    // Return false if we're scrolling in the x direction
    class YScrollDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {
            return (Math.abs(distanceY) > Math.abs(distanceX));
        }
    }
}
