package ru.onvolga.basiccomponents.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import ru.onvolga.basiccomponents.utils.TypefaceSpan2;

/**
 * Created by dzmitry on 27.2.17.
 */

public class MenuIconTextView extends TextView {
    public MenuIconTextView(Context context) {
        super(context);
        init();
    }

    public MenuIconTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MenuIconTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init() {
        setTypeface(TypefaceSpan2.getTypeFace(getContext()));
    }
}
