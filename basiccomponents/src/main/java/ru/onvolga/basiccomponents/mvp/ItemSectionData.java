package ru.onvolga.basiccomponents.mvp;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by dzmitry on 27.2.17.
 */

public class ItemSectionData {
    int id;
    boolean activity;
    int user_id;
    int _lft;
    int _rgt;
    Integer parent_id;
    String name;
    String slug;
    String introduction;
    String description;
    String image;
    String created_at;
    String updated_at;
    List<ItemSectionDataItem> items;

    public List<ItemSectionDataItem> getItems() {
        return items;
    }

    public void setItems(List<ItemSectionDataItem> items) {
        this.items = items;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public int get_rgt() {
        return _rgt;
    }

    public void set_rgt(int _rgt) {
        this._rgt = _rgt;
    }

    public int get_lft() {
        return _lft;
    }

    public void set_lft(int _lft) {
        this._lft = _lft;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public boolean isActivity() {
        return activity;
    }

    public void setActivity(boolean activity) {
        this.activity = activity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static class ItemSectionDataItem implements Parcelable {
        public static final Creator<ItemSectionDataItem> CREATOR = new Creator<ItemSectionDataItem>() {
            @Override
            public ItemSectionDataItem createFromParcel(Parcel in) {
                return new ItemSectionDataItem(in);
            }

            @Override
            public ItemSectionDataItem[] newArray(int size) {
                return new ItemSectionDataItem[size];
            }
        };
        int id;
        boolean activity;
        int section_id;
        int user_id;
        String name;
        String slug;
        String image;
        String introduction;
        String content;
        String created_at;
        String updated_at;
        PublicshedInfo published_at;

        protected ItemSectionDataItem(Parcel in) {
            id = in.readInt();
            activity = in.readByte() != 0;
            section_id = in.readInt();
            user_id = in.readInt();
            name = in.readString();
            slug = in.readString();
            image = in.readString();
            introduction = in.readString();
            content = in.readString();
            created_at = in.readString();
            updated_at = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeByte((byte) (activity ? 1 : 0));
            dest.writeInt(section_id);
            dest.writeInt(user_id);
            dest.writeString(name);
            dest.writeString(slug);
            dest.writeString(image);
            dest.writeString(introduction);
            dest.writeString(content);
            dest.writeString(created_at);
            dest.writeString(updated_at);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isActivity() {
            return activity;
        }

        public void setActivity(boolean activity) {
            this.activity = activity;
        }

        public int getSection_id() {
            return section_id;
        }

        public void setSection_id(int section_id) {
            this.section_id = section_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIntroduction() {
            return introduction;
        }

        public void setIntroduction(String introduction) {
            this.introduction = introduction;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public PublicshedInfo getPublished_at() {
            return published_at;
        }

        public void setPublished_at(PublicshedInfo published_at) {
            this.published_at = published_at;
        }

    }

    public static class PublicshedInfo {
        private String date;
        private String timezone;
        private int timezone_type;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

        public int getTimezone_type() {
            return timezone_type;
        }

        public void setTimezone_type(int timezone_type) {
            this.timezone_type = timezone_type;
        }
    }
}
