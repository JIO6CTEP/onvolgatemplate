package ru.onvolga.basiccomponents.mvp;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dzmitry on 27.2.17.
 */

public class PageElement extends RealmObject implements Parcelable {
    public static final Creator<PageElement> CREATOR = new Creator<PageElement>() {
        @Override
        public PageElement createFromParcel(Parcel in) {
            return new PageElement(in);
        }

        @Override
        public PageElement[] newArray(int size) {
            return new PageElement[size];
        }
    };
    @PrimaryKey
    private
    int id;
    private String name;
    private int sort_order;
    private boolean is_displayed_in_menu;
    private boolean is_displayed_in_main_page;
    private boolean is_displayed_in_bottom_menu;
    private int application_id;
    private String view_id;
    private String created_at;
    private String updated_at;
    private String icon;
    private boolean active;
    private String slug;
    //options
    private String data_type;
    private String data;

    public PageElement() {
    }

    protected PageElement(Parcel in) {
        id = in.readInt();
        name = in.readString();
        sort_order = in.readInt();
        is_displayed_in_menu = in.readByte() != 0;
        is_displayed_in_main_page = in.readByte() != 0;
        is_displayed_in_bottom_menu = in.readByte() != 0;
        application_id = in.readInt();
        view_id = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        icon = in.readString();
        active = in.readByte() != 0;
        slug = in.readString();
        data_type = in.readString();
        data = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(sort_order);
        dest.writeByte((byte) (is_displayed_in_menu ? 1 : 0));
        dest.writeByte((byte) (is_displayed_in_main_page ? 1 : 0));
        dest.writeByte((byte) (is_displayed_in_bottom_menu ? 1 : 0));
        dest.writeInt(application_id);
        dest.writeString(view_id);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(icon);
        dest.writeByte((byte) (active ? 1 : 0));
        dest.writeString(slug);
        dest.writeString(data_type);
        dest.writeString(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getView_id() {
        return view_id;
    }

    public void setView_id(String view_id) {
        this.view_id = view_id;
    }

    public int getApplication_id() {
        return application_id;
    }

    public void setApplication_id(int application_id) {
        this.application_id = application_id;
    }

    public boolean is_displayed_in_bottom_menu() {
        return is_displayed_in_bottom_menu;
    }

    public void setIs_displayed_in_bottom_menu(boolean is_displayed_in_bottom_menu) {
        this.is_displayed_in_bottom_menu = is_displayed_in_bottom_menu;
    }

    public boolean is_displayed_in_main_page() {
        return is_displayed_in_main_page;
    }

    public void setIs_displayed_in_main_page(boolean is_displayed_in_main_page) {
        this.is_displayed_in_main_page = is_displayed_in_main_page;
    }

    public boolean is_displayed_in_menu() {
        return is_displayed_in_menu;
    }

    public void setIs_displayed_in_menu(boolean is_displayed_in_menu) {
        this.is_displayed_in_menu = is_displayed_in_menu;
    }

    public int getSort_order() {
        return sort_order;
    }

    public void setSort_order(int sort_order) {
        this.sort_order = sort_order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
