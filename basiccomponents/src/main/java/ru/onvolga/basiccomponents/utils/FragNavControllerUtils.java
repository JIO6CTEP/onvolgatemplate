package ru.onvolga.basiccomponents.utils;

import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import ru.onvolga.basiccomponents.fragments.ElementsTestFragment;
import ru.onvolga.basiccomponents.fragments.EmptyElementFragment;
import ru.onvolga.basiccomponents.fragments.ItemSectionFragment;
import ru.onvolga.basiccomponents.fragments.ItemsSectionFragment;
import ru.onvolga.basiccomponents.mvp.PageElement;

/**
 * Created by dzmitry on 27.2.17.
 */

public class FragNavControllerUtils {
    public static List<Fragment> getRootMain(RealmResults<PageElement> list) {
        List<Fragment> result = new ArrayList<>();
        for (PageElement pageElement : list) {
            Fragment fragment = getRootFragment(pageElement);
            if (fragment != null)
                result.add(fragment);
        }

        if (result.size() == 0)
            result.add(new ElementsTestFragment());
        return result;
    }

    public static Fragment getRootFragment(PageElement element) {
        switch (element.getData_type()) {
            case "items_section":
                return ItemsSectionFragment.newInstance(element.getId());
            case "item":
                return ItemSectionFragment.newInstance(element.getId());

            case "empty":
            default:
                return EmptyElementFragment.newInstance(element.getId());
        }
    }
}
