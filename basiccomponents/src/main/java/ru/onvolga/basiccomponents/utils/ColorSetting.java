package ru.onvolga.basiccomponents.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.ColorInt;

/**
 * Created by dzmitry on 24.2.17.
 */

public class ColorSetting {

    private final static String CURRENT_SETTING_SHARED_PREF = "CURRENT_SETTING_SHARED_PREF";
    private final static String CURRENT_SETTING_VALUE = "CURRENT_SETTING_VALUE";

    public static void save(Context ctx, Colors color) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(CURRENT_SETTING_SHARED_PREF, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(CURRENT_SETTING_VALUE, color.toString()).commit();
    }

    public static Colors load(Context ctx) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(CURRENT_SETTING_SHARED_PREF, Context.MODE_PRIVATE);
        return Colors.fromString(sharedPreferences.getString(CURRENT_SETTING_VALUE, Colors.GREEN.toString()));
    }

    @ColorInt
    public static int getColor(Context ctx, COLORTYPES colortype) {
        return 0;
    }

    public enum COLORTYPES {
        MAIN,
        MAIN_DARK,
        BODY_BACK,
        BODY_BACK_CLICKED,
        SUBMAIN_BACK,
        DUBMAIN_BACK_CLICKED
    }

    public enum Colors {
        AQUA("aqua"),
        BEIGE("beige"),
        BLACK("black"),
        BLUE("blue"),
        BROWN("brown"),
        DARKBLUE("darkblue"),
        DARKGREEN("darkgreen"),
        DARKORCHID("darkorhid"),
        DARKPINK("darkpink"),
        DARKDARK("darkdark"),
        GOLD("gold"),
        GRAY("gray"),
        GREEN("green"),
        IZUMRUD("izumrud"),
        LIGCHTBLUE("lightblue"),
        LIGCHTBROWN("lightbrown"),
        LIGHTGRAY("lightgray"),
        LIGHTGREEN("lightgreen"),
        LIGHTPINK("lightpink"),
        MAGENTO("magento"),
        ORCHID("orchid"),
        PURPLE("purpule"),
        RED("red"),
        SAND("sand"),
        TURQUOISE("turquoise"),
        VIOLET("violet"),
        WHITE("white"),
        YELLOW("yellow");
        private final String text;

        /**
         * @param text
         */
        private Colors(final String text) {
            this.text = text;
        }

        public static Colors fromString(String text) {
            for (Colors b : Colors.values()) {
                if (b.text.equalsIgnoreCase(text)) {
                    return b;
                }
            }
            //throw new IllegalArgumentException("No constant with text " + text + " found");
            return GREEN;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }
}
