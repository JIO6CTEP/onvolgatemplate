package ru.onvolga.basiccomponents.utils;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.realm.Realm;
import io.realm.RealmList;
import ru.onvolga.basiccomponents.mvp.PageElement;

/**
 * Created by dzmitry on 23.2.17.
 */

public class ModuleSetting {

    public static ModuleSettingValue from(String s) {

        JsonParser parser = new JsonParser();
        JsonObject object = parser.parse(s).getAsJsonObject();
        RealmList<PageElement> resultPages = null;
        if (object.has("pages")) {
            JsonArray pages = object.getAsJsonArray("pages");
            for (JsonElement e : pages) {
                JsonObject value = e.getAsJsonObject();
                String data = null;
                if (value.has("data")) {
                    data = value.get("data").toString();
                    value.remove("data");
                }
                if (data != null)
                    value.addProperty("data", data);
            }
        }
        ModuleSettingValue result = new Gson().fromJson(object, ModuleSettingValue.class);
        Realm realm = Realm.getDefaultInstance();
        try {
            realm.beginTransaction();
            realm.delete(PageElement.class);
            realm.commitTransaction();
            realm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(result.pages));
        } finally {
            realm.close();
        }

        result.setPages(resultPages);
        return result;

    }

    public static class ModuleSettingValue implements Parcelable {
        public static final Creator<ModuleSettingValue> CREATOR = new Creator<ModuleSettingValue>() {
            @Override
            public ModuleSettingValue createFromParcel(Parcel in) {
                return new ModuleSettingValue(in);
            }

            @Override
            public ModuleSettingValue[] newArray(int size) {
                return new ModuleSettingValue[size];
            }
        };
        private String name;
        private String color;
        private RealmList<PageElement> pages;

        public ModuleSettingValue() {
        }

        protected ModuleSettingValue(Parcel in) {
            name = in.readString();
            color = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(color);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public RealmList<PageElement> getPages() {
            return pages;
        }

        public void setPages(RealmList<PageElement> pages) {
            this.pages = pages;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }
}
