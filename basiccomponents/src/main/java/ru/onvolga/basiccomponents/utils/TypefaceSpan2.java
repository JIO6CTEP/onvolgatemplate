package ru.onvolga.basiccomponents.utils;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

/**
 * Created by dzmitry on 13.7.16.
 */
//https://habrahabr.ru/post/231203/

public class TypefaceSpan2 extends MetricAffectingSpan {
    private static Typeface menuTypeFace = null;
    private final Typeface mTypeface;

    public TypefaceSpan2(Typeface typeface) {
        mTypeface = typeface;
    }

    private static void apply(Paint paint, Typeface tf) {
        int oldStyle;

        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }

        int fake = oldStyle & ~tf.getStyle();

        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }

        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        paint.setTypeface(tf);
    }

    public static Typeface getTypeFace(Context ctx) {
        if (menuTypeFace == null)
            menuTypeFace =
                    Typeface.createFromAsset(ctx.getAssets(), "fonts/profseller_app.ttf");
        return menuTypeFace;
    }

    public static SpannableStringBuilder getProfsellerIconsSpanable(Context ctx, String s) {
        if (menuTypeFace == null)
            menuTypeFace =
                    Typeface.createFromAsset(ctx.getAssets(), "fonts/profseller_app.ttf");

        SpannableStringBuilder resultSpan = new SpannableStringBuilder(s);
        TypefaceSpan2 roubleTypefaceSpan = new TypefaceSpan2(menuTypeFace);
        resultSpan.setSpan(roubleTypefaceSpan, 0, resultSpan.length(), 0);
        /*for (int i = 0; i < resultSpan.length(); i++) {
            if (resultSpan.charAt(i) == '\u20BD') {
                TypefaceSpan2 roubleTypefaceSpan = new TypefaceSpan2(menuTypeFace);
                resultSpan.setSpan(roubleTypefaceSpan, i, i + 1, 0);
            }
        }*/
        return resultSpan;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        apply(ds, mTypeface);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        apply(paint, mTypeface);
    }
}