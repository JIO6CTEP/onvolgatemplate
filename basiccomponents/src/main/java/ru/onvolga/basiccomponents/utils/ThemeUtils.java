package ru.onvolga.basiccomponents.utils;

import android.app.Activity;

import ru.onvolga.basiccomponents.R;

/**
 * Created by dzmitry on 24.2.17.
 */

public class ThemeUtils {
    public static void setTheme(Activity a, ColorSetting.Colors color){
        switch (color){
            case GREEN: a.setTheme(R.style.BasicAppTheme_Green); break;
            case BLUE: a.setTheme(R.style.BasicAppTheme_Blue); break;
        }
    }
}
