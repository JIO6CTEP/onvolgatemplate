package ru.onvolga.basiccomponents.utils;

/**
 * Created by dzmitry on 27.2.17.
 */

public class TextIconSimulate {
    public static String getMenuIcon(String string_code) {
        switch (string_code) {
            case "users": return "\ue800";
            case "right-open": return "\ue804";
            case "location": return "\ue808";
            case "th": return "\ue80c";
            case "up-dir": return "\ue810";
            case "basket": return "\ue814";
            case "right-open-1": return "\ue818";
            case "help-circled": return "\ue81c";
            case "help-circled-2": return "\ue820";
            case "info": return "\ue824";
            case "map": return "\ue828";
            case "money": return "\uf0d6";
            case "mail-alt": return "\uf0e0";
            case "pound": return "\uf154";
            case "rouble": return "\uf158";
            case "mail-squared": return "\uf199";
            case "percent": return "\uf295";
            case "lock": return "\ue801";
            case "left-open": return "\ue805";
            case "clock": return "\ue809";
            case "search": return "\ue80d";
            case "down-dir": return "\ue811";
            case "home-outline": return "\ue815";
            case "left-open-1": return "\ue819";
            case "help": return "\ue81d";
            case "truck": return "\ue821";
            case "info-circled-1": return "\ue825";
            case "location-1": return "\ue829";
            case "sort": return "\uf0dc";
            case "doc-text": return "\uf0f6";
            case "dollar": return "\uf155";
            case "won": return "\uf159";
            case "cab": return "\uf1b9";
            case "question-circle-o": return "\uf29c";
            case "user": return "\ue802";
            case "th-large": return "\ue806";
            case "info-circled": return "\ue80a";
            case "plus": return "\ue80e";
            case "cancel": return "\ue812";
            case "down-open": return "\ue816";
            case "doc": return "\ue81a";
            case "help-circled-1": return "\ue81e";
            case "home": return "\ue822";
            case "mail-1": return "\ue826";
            case "location-circled": return "\ue82a";
            case "sort-down": return "\uf0dd";
            case "info-1": return "\uf129";
            case "rupee": return "\uf156";
            case "bitcoin": return "\uf15a";
            case "cart-plus": return "\uf217";
            case "phone": return "\ue803";
            case "mail": return "\ue807";
            case "file-pdf": return "\ue80b";
            case "minus": return "\ue80f";
            case "ok": return "\ue813";
            case "up-open": return "\ue817";
            case "doc-1": return "\ue81b";
            case "help-1": return "\ue81f";
            case "wallet": return "\ue823";
            case "mail-2": return "\ue827";
            case "menu": return "\uf0c9";
            case "sort-up": return "\uf0de";
            case "euro": return "\uf153";
            case "yen": return "\uf157";
            case "doc-text-inv": return "\uf15c";
            case "rt-arrow-down": return "\uf218";
            default:
                return "";
        }
    }
}
