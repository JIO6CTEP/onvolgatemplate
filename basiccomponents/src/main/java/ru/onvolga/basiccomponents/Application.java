package ru.onvolga.basiccomponents;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by dzmitry on 23.2.17.
 */

public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
