package ru.onvolga.basiccomponents;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import ru.onvolga.basiccomponents.activities.BaseActivity;
import ru.onvolga.basiccomponents.activities.MainActivity;
import ru.onvolga.basiccomponents.utils.ColorSetting;
import ru.onvolga.basiccomponents.utils.ModuleSetting;

public class StartActivity extends BaseActivity {

    public final static String MODULE_SETTING = "ru.onvolga.basiccomponents.StartActivity.MODULE_SETTING";

    @BindView(R2.id.textView)
    TextView textView;

    @BindView(R2.id.button_go)
    Button button_go;

    @OnClick(R2.id.button_go)
    void onButtonGoClick() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ModuleSetting.ModuleSettingValue moduleSetting = ModuleSetting.from(getIntent().getStringExtra(MODULE_SETTING));
        ColorSetting.save(this, ColorSetting.Colors.fromString(moduleSetting.getColor()));
        applyTheme();
        setContentView(R.layout.activity_start);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
