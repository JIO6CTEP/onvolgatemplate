package ru.onvolga.basiccomponents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.onvolga.basiccomponents.R;
import ru.onvolga.basiccomponents.views.DisableScrollView;


/**
 * Created by sdim on 22.09.2016.
 */
public abstract class ScrollableFragment extends BaseFragment {
    protected DisableScrollView contentPanel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentPanel = (DisableScrollView) inflater.inflate(R.layout.fragment_scrollable_default, container, false);
        View v = createView(inflater, contentPanel, savedInstanceState);
        contentPanel.addView(v);
        return contentPanel;
    }

    protected abstract View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
}
