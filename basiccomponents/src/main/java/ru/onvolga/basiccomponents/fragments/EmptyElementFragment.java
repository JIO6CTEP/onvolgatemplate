package ru.onvolga.basiccomponents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.onvolga.basiccomponents.R;
import ru.onvolga.basiccomponents.mvp.PageElement;

/**
 * Created by dzmitry on 25.2.17.
 */

public class EmptyElementFragment extends PageElementFragment {
    public static EmptyElementFragment newInstance(int page_id) {
        Bundle args = new Bundle();
        addPageElement(args, page_id);
        EmptyElementFragment fragment = new EmptyElementFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_elemets_test2, container, false);
    }
}
