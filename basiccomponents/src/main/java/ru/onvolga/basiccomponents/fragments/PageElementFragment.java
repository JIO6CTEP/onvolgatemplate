package ru.onvolga.basiccomponents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import io.realm.Realm;
import ru.onvolga.basiccomponents.mvp.PageElement;

/**
 * Created by dzmitry on 27.2.17.
 */

public abstract class PageElementFragment extends BaseFragment {
    private final static String PAGE_ELEMENT_ID = "PAGE_ELEMENT_ID";
    PageElement pageElement;
    Realm realm;

    public static void addPageElement(Bundle arg, int page_id) {
        arg.putInt(PAGE_ELEMENT_ID, page_id);
    }

    public int getPageId() {
        if (pageElement != null)
            return pageElement.getId();
        return -1;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (pageElement != null)
            getActivity().setTitle(pageElement.getName());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null)
            if(getArguments().getInt(PAGE_ELEMENT_ID,-1)!=-1)
            pageElement = realm.where(PageElement.class).equalTo("id", getArguments().getInt(PAGE_ELEMENT_ID)).findFirst();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
