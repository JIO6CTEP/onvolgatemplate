package ru.onvolga.basiccomponents.fragments;

import android.os.Bundle;

/**
 * Created by dzmitry on 27.2.17.
 */

public class ItemOneSectionFragment extends PageElementFragment {
    public static ItemOneSectionFragment newInstance(int page_id) {
        Bundle args = new Bundle();
        addPageElement(args, page_id);
        ItemOneSectionFragment fragment = new ItemOneSectionFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
