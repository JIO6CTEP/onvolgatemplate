package ru.onvolga.basiccomponents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.onvolga.basiccomponents.R;
import ru.onvolga.basiccomponents.R2;

/**
 * Created by dzmitry on 25.2.17.
 */

public class ElementsTestFragment extends BaseFragment {
    @OnClick(R2.id.testButton)
    void testButton() {
        mFragmentNavigation.pushFragment(new EmptyElementFragment());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_elemets_test, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }
}
