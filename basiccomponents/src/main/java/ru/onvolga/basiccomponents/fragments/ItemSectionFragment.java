package ru.onvolga.basiccomponents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.onvolga.basiccomponents.R;
import ru.onvolga.basiccomponents.R2;
import ru.onvolga.basiccomponents.mvp.ItemSectionData;

/**
 * Created by dzmitry on 28.2.17.
 */

public class ItemSectionFragment extends PageScrollableElementFragment {

    private final static String ITEM_SECTION_DATA = "ITEM_SECTION_DATA";
    ItemSectionData.ItemSectionDataItem currItemSectionDataItem;

    @BindView(R2.id.nameTextView)
    TextView nameTextView;

    @BindView(R2.id.contentTextView)
    TextView contentTextView;

    @BindView(R2.id.itemImage)
    ImageView itemImage;

    public static ItemSectionFragment newInstance(ItemSectionData.ItemSectionDataItem itemSectionDataItem) {
        Bundle args = new Bundle();
        args.putParcelable(ITEM_SECTION_DATA, itemSectionDataItem);
        ItemSectionFragment fragment = new ItemSectionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ItemSectionFragment newInstance(int page_id) {
        Bundle args = new Bundle();
        addPageElement(args, page_id);
        ItemSectionFragment fragment = new ItemSectionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (pageElement == null && currItemSectionDataItem != null)
            mActivity.setTitle(currItemSectionDataItem.getName());
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_section, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        prepareView();
    }

    private void prepareView() {
        if (pageElement != null) {
            currItemSectionDataItem = new Gson().fromJson(pageElement.getData(), ItemSectionData.ItemSectionDataItem.class);
        } else
            currItemSectionDataItem = getArguments().getParcelable(ITEM_SECTION_DATA);
        if (currItemSectionDataItem == null) return;
        nameTextView.setText(currItemSectionDataItem.getName());
        contentTextView.setText(currItemSectionDataItem.getContent());

    }
}
