package ru.onvolga.basiccomponents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.onvolga.basiccomponents.R;
import ru.onvolga.basiccomponents.R2;
import ru.onvolga.basiccomponents.adapters.ItemSectionDataItemAdapter;
import ru.onvolga.basiccomponents.mvp.ItemSectionData;

/**
 * Created by dzmitry on 27.2.17.
 */

public class ItemsSectionFragment extends PageElementFragment implements ItemSectionDataItemAdapter.OnItemSectionClickListener {
    @BindView(R2.id.recyclerView)
    RecyclerView recyclerView;

    public static ItemsSectionFragment newInstance(int page_id) {
        Bundle args = new Bundle();
        addPageElement(args, page_id);
        ItemsSectionFragment fragment = new ItemsSectionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    void prepareItems() {
        String s = pageElement.getData();
        ItemSectionData itemSectionData = new Gson().fromJson(pageElement.getData(), ItemSectionData.class);
        if (itemSectionData != null) {
            ItemSectionDataItemAdapter adapter = new ItemSectionDataItemAdapter(mActivity, itemSectionData.getItems());
            adapter.setOnItemSectionClickListener(this);
            recyclerView.setAdapter(adapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_items_selection, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        prepareItems();

    }

    @Override
    public void onItemClick(ItemSectionData.ItemSectionDataItem item) {
        mFragmentNavigation.pushFragment(ItemSectionFragment.newInstance(item));
    }
}
