package ru.onvolga.basiccomponents.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ru.onvolga.basiccomponents.utils.ColorSetting;
import ru.onvolga.basiccomponents.utils.ThemeUtils;

/**
 * Created by dzmitry on 24.2.17.
 */

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        applyTheme();
    }

    protected void applyTheme() {
        ThemeUtils.setTheme(this, ColorSetting.load(this));
    }
}
