package ru.onvolga.basiccomponents.activities;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.ncapdevi.fragnav.FragNavController;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import ru.onvolga.basiccomponents.R;
import ru.onvolga.basiccomponents.R2;
import ru.onvolga.basiccomponents.adapters.IOnPageElementClickListener;
import ru.onvolga.basiccomponents.adapters.MainBottomMenuAdapter;
import ru.onvolga.basiccomponents.adapters.MainNavMenuAdapter;
import ru.onvolga.basiccomponents.fragments.BaseFragment;
import ru.onvolga.basiccomponents.fragments.PageElementFragment;
import ru.onvolga.basiccomponents.mvp.PageElement;
import ru.onvolga.basiccomponents.utils.FragNavControllerUtils;

/**
 * Created by dzmitry on 25.2.17.
 */

public class MainActivity extends BaseActivity implements BaseFragment.FragmentNavigation, IOnPageElementClickListener {

    private final static String BOTTOM_SELECTED_PAGE_ID = "BOTTOM_SELECTED_PAGE_ID";

    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R2.id.menuList)
    RecyclerView menuList;
    @BindView(R2.id.bottomMenu)
    RecyclerView bottomMenu;

    MainBottomMenuAdapter bottomMenuAdapter;

    private FragNavController mNavController;

    private Realm realm;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        menuList.setAdapter(null);
        realm.close();
    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        menuList.setLayoutManager(new LinearLayoutManager(this));
        RealmResults<PageElement> leftMenuResults = realm.where(PageElement.class).equalTo("is_displayed_in_menu", true).equalTo("active", true).findAllSorted("sort_order");
        RealmResults<PageElement> bottomMenuResults = realm.where(PageElement.class).equalTo("is_displayed_in_bottom_menu", true).equalTo("active", true).findAllSorted("sort_order");
        RealmResults<PageElement> rootMenuResults = realm.where(PageElement.class).beginGroup().equalTo("is_displayed_in_bottom_menu", true).or().equalTo("is_displayed_in_menu", true).endGroup().equalTo("active", true).findAllSorted("sort_order");
        MainNavMenuAdapter adapter = new MainNavMenuAdapter(this, leftMenuResults);
        adapter.setListener(this);
        menuList.setAdapter(adapter);
        bottomMenuAdapter = new MainBottomMenuAdapter(this, bottomMenuResults);
        bottomMenuAdapter.setListener(this);

        bottomMenu.setLayoutManager(new GridLayoutManager(this, bottomMenuResults != null ? bottomMenuResults.size() : 1) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }

            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        });
        bottomMenu.setAdapter(bottomMenuAdapter);
        if (savedInstanceState != null)
            bottomMenuAdapter.setPageSelected(savedInstanceState.getInt(BOTTOM_SELECTED_PAGE_ID));
        else
            bottomMenuAdapter.setPageSelected((bottomMenuResults != null && bottomMenuResults.size() > 0) ? bottomMenuResults.get(0).getId() : -1);
        // ((DrawerLayout)findViewById(R.id.drawer_layout)).openDrawer(Gravity.START);
        List<Fragment> rootFragments = FragNavControllerUtils.getRootMain(rootMenuResults);

        mNavController = new FragNavController(savedInstanceState, getSupportFragmentManager(), R.id.container, rootFragments, FragNavController.TAB1);
        mNavController.setTransitionMode(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        mNavController.setTransactionListener(new FragNavController.TransactionListener() {
            @Override
            public void onTabTransaction(Fragment fragment, int i) {

            }

            @Override
            public void onFragmentTransaction(Fragment fragment) {
                if (fragment instanceof PageElementFragment) {
                    int pageSelected = ((PageElementFragment) fragment).getPageId();
                    if (pageSelected != -1)
                        bottomMenuAdapter.setPageSelected(pageSelected);
                }
            }
        });
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (mNavController.getCurrentStack().size() > 1) {
            mNavController.popFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (bottomMenuAdapter != null)
            outState.putInt(BOTTOM_SELECTED_PAGE_ID, bottomMenuAdapter.getPageSelected());
        mNavController.onSaveInstanceState(outState);
    }

    @Override
    public void pushFragment(Fragment fragment) {
        mNavController.pushFragment(fragment);
    }

    @Override
    public void onPageClick(PageElement page) {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        mNavController.clearStack();
        pushFragment(FragNavControllerUtils.getRootFragment(page));
    }
}
