package ru.onvolga.u123456_a654321;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;

import ru.onvolga.basiccomponents.StartActivity;
import ru.onvolga.basiccomponents.utils.ModuleSetting;

public class MainActivity extends AppCompatActivity {

    private final static String FILENAME = "setting.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, StartActivity.class);
        intent.putExtra(StartActivity.MODULE_SETTING, getModuleSetting());
        startActivity(intent);
        finish();
    }

    private String getModuleSetting() {
        String text = FILENAME;
        byte[] buffer = null;
        InputStream is;
        try {
            is = getAssets().open(text);
            int size = is.available();
            buffer = new byte[size];
            is.read(buffer);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String str_data = new String(buffer);
        return str_data;
    }
}
